package plotplanner;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OperationsTest {

	private ArrayList<ArrayList<SimplePlot>> storage;
	private ArrayList<ArrayList<SimplePlot>> storageTested;
	
	@BeforeEach
	void setUp() {
		storage = new ArrayList<ArrayList<SimplePlot>> () ;
		ArrayList<SimplePlot> tempSimplePlotStorage = new ArrayList<SimplePlot>();
		
		SimplePlot s1 = new SimplePlot();
		//adds 3 plots in the first storage array; a plotLength of 3
		tempSimplePlotStorage.add(s1);
		tempSimplePlotStorage.add(s1);
		tempSimplePlotStorage.add(s1);
		
		//adds first storage array of already filled plots; creates a plotWidth of 1
		storage.add(tempSimplePlotStorage);
		
		// s1 = new SimplePlot("Plot2");
		tempSimplePlotStorage = new ArrayList<SimplePlot>();
		//adds 3 plots in the second storage array; a matching plotLength of 3
		tempSimplePlotStorage.add(s1);
		tempSimplePlotStorage.add(s1);
		tempSimplePlotStorage.add(s1);
		
		//adds second storage array of already filled plots; creates a plotWidth of 2
		storage.add(tempSimplePlotStorage);	
	}
	
	//tests that the created container is the correct dimensions
	@Test
	void testCreateSpace() {
		
		storageTested = new ArrayList<ArrayList<SimplePlot>> () ;
		
		storageTested = Operations.createSpace(2, 3);
		
		assertEquals(2, storageTested.size() );
		assertEquals(3, storageTested.get(0).size() );
		assertEquals(3, storageTested.get(1).size() );
		
	}

	@Test
	void testPrintPlot() {
		fail("Not yet implemented");
	}

	@Test
	void testUpdatePlotName() {
		fail("Not yet implemented");
	}

	@Test
	void testUpdatePlot() {
		fail("Not yet implemented");
	}

	@Test
	void testVisualPrint() {
		fail("Not yet implemented");
	}

	@Test
	void testNewVisualPrint() {
		fail("Not yet implemented");
	}

}
