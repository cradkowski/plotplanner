package plotplanner;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SimplePlotTest {

	private SimplePlot s1;
	
	@BeforeEach
	void setUp() {
		s1 = new SimplePlot("Plot1");
	}
	
	
	@Test
	void testSimplePlot() {
		s1 = new SimplePlot("Plot2");
		assertEquals("Plot2", s1.getName() );
	}

	@Test
	void testSetCrop() {
		SimpleCrop c1 = new SimpleCrop("Jedi", "Pepper");
		s1.setCrop(c1);
		assertEquals(c1, s1.getCrop() );
	}

	@Test
	void testSetName() {
		s1.setName("Plot35");
		assertEquals("Plot35", s1.getName() );
	}

	@Test
	void testGetName() {
		assertEquals("Plot1", s1.getName() );
	}


	@Test
	void testToString() {
		SimpleCrop c1 = new SimpleCrop("Jedi", "Pepper");
		s1.setCrop(c1);
		assertEquals("Plot1 Jedi Pepper", s1.toString() );
	}

}
