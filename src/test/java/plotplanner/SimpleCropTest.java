package plotplanner;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SimpleCropTest {
	
	private SimpleCrop c1;
	
	@BeforeEach
	void setUp() {
		c1 = new SimpleCrop("Jedi", "Pepper");
	}

	@Test
	void testSimpleCrop() {
		c1 = new SimpleCrop("Buttercrunch", "Lettuce");
		assertEquals("Buttercrunch", c1.getName() );
	}

	@Test
	void testGetFamily() {
		assertEquals("Pepper", c1.getFamily() );
	}

	@Test
	void testToString() {
		assertEquals("Jedi Pepper", c1.toString() );
	}

}
