package plotplanner;

public class SimplePlot {
	
	
	private String name = "";
	private SimpleCrop crop;
	
	public SimplePlot() {
		
	}
	
	
	public SimplePlot(String name) {
		this.name = name;
	}
	
	public void setCrop(SimpleCrop crop) {
		this.crop = crop;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public String getName() {
		return this.name;
	}
	
	public SimpleCrop getCrop() { 
		return this.crop;
	}
	
	public String toString() {
		return name + " " + crop;
	}
	
	
	
	

}
