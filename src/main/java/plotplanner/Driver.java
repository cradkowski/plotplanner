

package plotplanner;

import java.util.*;
import java.awt.Point;



public class Driver {

	
	
	public static void main (String args[] ) {
		
		SimpleCrop c1 = new SimpleCrop("Buttercrunch", "Lettuce");
		
		SimpleCrop c2 = new SimpleCrop("Jedi", "Pepper");
		
		SimplePlot sp1 = new SimplePlot("Plot1");
		
		SimplePlot sp2 = new SimplePlot("Plot2");
		
		sp1.setCrop(c1);
		
		sp2.setCrop(c2);
		
		//https://stackoverflow.com/questions/10866205/two-dimensional-array-list
		
		ArrayList<ArrayList<SimplePlot>> storage;
		
		storage = Operations.createSpace(2, 8);
		
		Operations.printPlot(storage);
		
		storage = Operations.updatePlot(storage, sp1, 2, 8);
		
		Operations.printPlot(storage);
		
		storage = Operations.updatePlot(storage, sp2, 1, 4);
		
		Operations.printPlot(storage);
		
		// Operations.visualPrint(storage);
		
		// Operations.newVisualPrint(storage);
		
	}
	
	
}
