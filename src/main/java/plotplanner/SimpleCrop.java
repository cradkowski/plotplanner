package plotplanner;

public class SimpleCrop {
	
	
	private String name = "";
	private String family = "";
	
	
	
	public SimpleCrop (String name, String family) {
		this.name = name;
		this.family = family;
	}
	
	public String getFamily() { 
		return this.family;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	public String toString() {
		return this.name + " " + this.family; 
	}
	

}
