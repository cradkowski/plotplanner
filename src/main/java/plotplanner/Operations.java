package plotplanner;

import java.util.ArrayList;

public class Operations {

	public static ArrayList<ArrayList<SimplePlot>> createSpace(int spaceWidth, int spaceLength) {
		
		ArrayList<ArrayList<SimplePlot>> storage = new ArrayList<ArrayList<SimplePlot>>();

		SimplePlot s1 = new SimplePlot();

		ArrayList<SimplePlot> temp;

		for (int i = 0; i < spaceWidth; i++) {

			temp = new ArrayList<SimplePlot>();

			for (int j = 0; j < spaceLength; j++) {
				temp.add(s1);
			}

			storage.add(i, temp);

		}

		return storage;

	}

	public static void printPlot(ArrayList<ArrayList<SimplePlot>> storage) {
		System.out.println(storage);
	}

	public static ArrayList<ArrayList<SimplePlot>> updatePlotName(ArrayList<ArrayList<SimplePlot>> storage,
			String oldName, String newName) {
		ArrayList<SimplePlot> temp;

		for (int i = 0; i < storage.size(); i++) {
			temp = storage.get(i);
			for (int j = 0; j < temp.size(); j++) {
				if (temp.get(j).getName() == oldName) {
					temp.get(j).setName(newName);
				}
			}

		}

		return storage;
	}

	public static ArrayList<ArrayList<SimplePlot>> updatePlot(ArrayList<ArrayList<SimplePlot>> storage, SimplePlot plot,
			int plotWidth, int plotLength) {
			
		ArrayList<SimplePlot> temp;
		for (int i = 0; i < plotWidth; i++) {
			temp = storage.get(i);
			for (int j = 0; j < plotLength; j++) {
				temp.set(j, plot);
			}
		}

		return storage;

	}

	public static void visualPrint(ArrayList<ArrayList<SimplePlot>> storage) {
		ArrayList<SimplePlot> temp;
		String output = "";
		String manipulator = "";

		System.out.println(storage.size());

		temp = storage.get(0); // TODO change this to be dynamic as having different sized containers (y
								// values) will cause this to either miss values or go out of bounds

		for (int i = 0; i < temp.size(); i++) {
			output += "y=" + i;
			output += "|";

			for (int j = 0; j < storage.size(); j++) {
				temp = storage.get(j);
				output += temp.get(i).getCrop().getFamily().charAt(0);

			}

			output += "|\n";
		}

		System.out.println("\n\n\n\n" + output);

	} // end method

	public static void newVisualPrint(ArrayList<ArrayList<SimplePlot>> storage) {
		System.out.println("NEW VISUAL PRINT METHOD:");

		ArrayList<ArrayList<String>> charStorage = new ArrayList<ArrayList<String>>();
		ArrayList<String> tempStringStorage;

		for (int i = 0; i < storage.size(); i++) {

			tempStringStorage = new ArrayList<String>();
			charStorage.add(i, tempStringStorage);

		}


		ArrayList<SimplePlot> temp;
		String output = "";
		// loop through "x" values
		for (int i = 0; i < storage.size(); i++) {
			temp = storage.get(i);
			for (int j = 0; j < temp.size(); j++) {
				charStorage.get(i).add(temp.get(j).getCrop().getFamily().charAt(0) + "");
			}

		}

		for (int k = 0; k < storage.size(); k++) {

			tempStringStorage = charStorage.get(k);

			String tempString = "";

			for (int i = 0; i < tempStringStorage.size(); i++) {
				tempString += tempStringStorage.get(i);
			}

			tempStringStorage = new ArrayList<String>();

			for (int i = tempString.length() - 1; i >= 0; i--) {
				tempStringStorage.add(tempString.charAt(i) + "");
			}

			charStorage.set(k, tempStringStorage);

		}

		tempStringStorage = charStorage.get(0); // TODO change this to be dynamic as having different sized containers (y
		// values) will cause this to either miss values or go out of bounds

		int y = tempStringStorage.size() - 1;
		
		for (int i = 0; i < tempStringStorage.size(); i++) {
			
			if (y < 10 )
				output+= "y=0" + y;
			else
			output += "y=" + y;
			output += "|";
			y--;

			for (int j = 0; j < charStorage.size(); j++) {
				tempStringStorage = charStorage.get(j);
				output += tempStringStorage.get(i).charAt(0);

			}

			output += "|\n";
		}

		System.out.println("\n" + output);

	} // end new print method

} // end class
